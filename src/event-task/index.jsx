import './index.css';

export const user = {
    email: '',
    password: '',
    loggedIn: false,
};
  
function App() {
    function clickHandler() {
      user.email = 'test@test.com';
      user.password = 123456;
      user.loggedIn = true;
      
      console.log(user);
    }

    return (
      <div id="app">
        <h1>User Login</h1>
        <p>
          <label>Email</label>
          <input type="email" />
        </p>
  
        <p>
          <label>Password</label>
          <input type="password" />
        </p>
  
  
        <p id="actions">
          <button onClick={clickHandler}>Login</button>
        </p>
      </div>
    );
}
  
export default App;
  