import './index.css';
import { useState } from 'react';

function App() {
  const [initialClass, setInitialClass] = useState('');

  function handleClick(value) {
    setInitialClass(value === 'yes' ? 'highlight-green' : 'highlight-red');
  }

    return (
      <div id="app">
        <h1 className={initialClass}>CSS is great!</h1>
        <menu>
          <li>
            <button onClick={() => handleClick('yes')}>Yes</button>
          </li>
          <li>
            <button onClick={() => handleClick('no')}>No</button>
          </li>
        </menu>
      </div>
    );
}
  
export default App;