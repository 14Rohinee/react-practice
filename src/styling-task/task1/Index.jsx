import { useState } from "react";
import './index.css';

function App() {
    const [initialColor, setInitialColor] = useState('white');

    function handleClick(value) {
        setInitialColor(value === 'yes' ? 'green' : 'red');
    }

    return (
        <div id="app">
            <h1 style={{ color: initialColor }}>CSS is great!</h1>
            <menu>
                <li>
                    <button onClick={() => handleClick('yes')}>Yes</button>
                </li>
                <li>
                    <button onClick={() => handleClick('no')}>No</button>
                </li>
            </menu>
        </div>
    );

}

export default App;
