import { useState } from "react";
import './index.css';

export default function App() {
    const [price, setPrice] = useState(100);

    function clickHandler() {
        setPrice(75);
    }

    return (
        <div>
            <p data-testid="price">${price}</p>
            <button onClick={clickHandler}>Apply Discount</button>
        </div>
    );
}