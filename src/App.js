// import Index from './reuse-component-task/index';
// import Index from './working-with-state/index';
import { Index } from './ref/forwarding-ref/index.jsx';

function App() {
  return (
    <div className="App">
      <Index />
    </div>
  );
}

export default App;
