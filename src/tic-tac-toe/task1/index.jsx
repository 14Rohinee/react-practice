import { useState } from 'react';
import './index.css';

export default function App() {
    const [isEditing, setIsEditing] = useState(false);

    function handleClick() {
        setIsEditing(true);
    }

    let content = <span className='player-name'>HELLO</span>;

    if (isEditing) {
        content = <input type='text'/>;
    }

    return (
        <>
            <h1>React Tic-Tac-Toe</h1>
            <li>
                <span className='player'>
                    {content} 
                    <span className='player-symbol'>Rohinee</span>
                </span>
                <button onClick={handleClick} type='button'>Edit</button>
            </li>
        </>
    )
}

