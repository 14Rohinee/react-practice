import React, { useState } from 'react';
import Review from './review';
import './index.css';

function App() {
    const [formData, setFormData] = useState({
        feedback: '',
        name: '',
    });

    const [reviewData, setReviewData] = useState({
        feedback: '',
        student: '',
    });

    const handleChange = (event) => {
        const { name, value } = event.target;
        setFormData({ ...formData, [name]: value });
    }

    const handleClick = () => {
        setReviewData({ feedback: formData.feedback, student: formData.name });
    }

    return (
        <>
            <section id="feedback">
                <h2>Please share some feedback</h2>
                <p>
                    <label>Your Feedback</label>
                    <textarea onChange={handleChange} value={formData.feedback} name='feedback'></textarea>
                </p>
                <p>
                    <label>Your Name</label>
                    <input type="text" value={formData.name} onChange={handleChange} name='name'/>
                </p>
            </section>
            <section id="draft">
                <h2>Your feedback</h2>
                <Review feedback={reviewData.feedback} student={reviewData.student}/>
                <p>
                    <button onClick={handleClick}>Save</button>
                </p>
            </section>
        </>
    );
}

export default App;
