import React from 'react';

const card = ({ name, para, link, title }) => {
    return (
        <div>
            <div name={name}>
                <p>{para}</p>
                <p>
                    <a href={link}>{title}</a>
                </p>
            </div>
        </div>
    );
};

export default card;