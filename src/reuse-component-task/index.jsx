import Card from './card';
import './index.css';

export default function App() {
  return (
    <div id="app" className='card'>
        <h1>Available Experts</h1>

        <Card name="Anthony Blake" para="Blake is a professor of Computer Science at the University of
            Illinois." link="mailto:blake@example.com" emailTitle="Email Anthony">
        </Card>
        <Card name="Maria Miles" para="Maria is a professor of Computer Science at the University of
            Illinois." link="mailto:blake@example.com" emailTitle="Email Maria">
        </Card>
    </div>
  );
}